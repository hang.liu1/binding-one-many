package com.twuc.webApp;


import com.twuc.webApp.domain.Office;
import com.twuc.webApp.domain.OfficeRepository;
import com.twuc.webApp.domain.Staff;
import com.twuc.webApp.domain.StaffRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;

import javax.persistence.EntityManager;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

@DataJpaTest(showSql = false)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class OneToManyTest {
    @Autowired
    private OfficeRepository officeRepository;
    
    @Autowired
    private StaffRepository staffRepository;
    
    @Autowired
    EntityManager entityManager;
    
    private void flushAndClear(Runnable run){
        run.run();
        entityManager.flush();
        entityManager.clear();
    }

    @Test
    void should_create_table() {
        flushAndClear(() -> {
            Office office = new Office("office1");
            Staff staff = new Staff("staff1");

            staff.setOffice(office);

            officeRepository.save(office);

        });



    }
}
